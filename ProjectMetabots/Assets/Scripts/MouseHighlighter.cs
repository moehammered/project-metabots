﻿using UnityEngine;
using System.Collections;

public class MouseHighlighter : MonoBehaviour {

	public GameObject effect;
	public GameObject currentlyHighlightedObject;
	public GameObject enemySelectedUI;
	public LayerMask mask;
	private Material effectMat;
	
	// Use this for initialization
	void Start () {
		effectMat = effect.GetComponent<Renderer>().material;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		checkUnderMouse();
	}
	
	void checkUnderMouse()
	{
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit info;
		
		if(Physics.Raycast(ray, out info, 100, mask))
		{
			effect.gameObject.SetActive (true);
//			print ("Object under mouse is: " + info.collider.name);
			highlightObject(info.transform.gameObject);
		}
		else
		{
			effect.gameObject.SetActive (false);
			currentlyHighlightedObject = null;
			enemySelectedUI.SetActive (false);
		}
	}
	
	void highlightObject(GameObject gb)
	{
		currentlyHighlightedObject = gb;
		effect.transform.parent = gb.transform;
		effect.transform.localPosition = new Vector3(0,1,0);
//		effectMat.color = determineColour(gb.tag);
		changeHighlightColour(determineColour(gb.tag));
	}
	
	void changeHighlightColour(Color col)
	{
//		effectMat.SetColor(1, col);
		effectMat.SetColor("_TintColor", col);
	}
	
	Color determineColour(string criteria)
	{
		enemySelectedUI.SetActive (false);
		switch(criteria)
		{
			case "Enemy":
				enemySelectedUI.SetActive (true);
				return Color.red;
			
			case "Trigger":
				return Color.green;
				
			default:
				return Color.clear;
		}
	}
}
