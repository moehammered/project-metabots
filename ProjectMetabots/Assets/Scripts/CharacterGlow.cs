﻿using UnityEngine;
using System.Collections;

public class CharacterGlow : MonoBehaviour {

	public float highVal, lowVal;
	public Renderer robotRen;
	public Color emissionColour = Color.white;
	private Material mat;
	
	// Use this for initialization
	void Start () {
		mat = robotRen.material;
		
		StartCoroutine(glowUp());
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	private IEnumerator glowUp()
	{
		float time = 0;
		float inten = 0;
		while(time < 1)
		{
			time += Time.deltaTime;
			inten = Mathf.Lerp(lowVal, highVal, time);
			mat.SetColor("_EmissionColor", emissionColour * inten);
//			DynamicGI.SetEmissive(robotRen, Color.red * inten);
//			DynamicGI.UpdateMaterials(robotRen);
			yield return null;
		}
		StartCoroutine(glowDown());
	}
	
	private IEnumerator glowDown()
	{
		float time = 0;
		float inten = 1;
		while(time < 1)
		{
			time += Time.deltaTime;
			inten = Mathf.Lerp(highVal, lowVal, time);
			mat.SetColor("_EmissionColor", emissionColour * inten);
//			DynamicGI.SetEmissive(robotRen, Color.red * inten);
//			DynamicGI.UpdateMaterials(robotRen);
			yield return null;
		}
		StartCoroutine(glowUp());
	}
}
