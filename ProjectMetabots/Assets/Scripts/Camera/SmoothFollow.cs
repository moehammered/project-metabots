﻿using UnityEngine;
using System.Collections;

public class SmoothFollow : MonoBehaviour {
	
	// The target we are following
	public Transform target;
	// The distance in the x-z plane to the target
	public float distance = 10.0f;
	// the height we want the camera to be above the target
	public float height = 5.0f;
	// How much we 
	public float heightDamping = 2.0f;
	public float rotationDamping = 3.0f;
	
	public bool useManualRotationAngle = false;
	public float manualRotationAngle = 180f;
	
	void LateUpdate()
	{
		// Early out if we don't have a target
		if (!target)
			return;
		
		// Calculate the current rotation angles
		var wantedRotationAngle = target.eulerAngles.y;
		var wantedHeight = target.position.y + height;
		
		var currentRotationAngle = transform.eulerAngles.y;
		var currentHeight = transform.position.y;
		
		// Damp the rotation around the y-axis
		currentRotationAngle = Mathf.LerpAngle (currentRotationAngle, wantedRotationAngle, rotationDamping * Time.deltaTime);
		
		// Damp the height
		currentHeight = Mathf.Lerp (currentHeight, wantedHeight, heightDamping * Time.deltaTime);
		
		Quaternion currentRotation;
		// Convert the angle into a rotation
		if(!useManualRotationAngle)
		{
			currentRotation = Quaternion.Euler(0, currentRotationAngle, 0);
		}
		else
		{
			currentRotation = Quaternion.Euler(0, manualRotationAngle, 0);
		}
		// Set the position of the camera on the x-z plane to:
		// distance meters behind the target
		
		
		Vector3 targetPos = target.position;
		targetPos -= currentRotation * Vector3.forward * distance;
		//targetPos -= transform.forward * distance;
		// Set the height of the camera
		targetPos.y = currentHeight;
		transform.position = targetPos;
		
		// Always look at the target
		transform.LookAt (target);
	}
}