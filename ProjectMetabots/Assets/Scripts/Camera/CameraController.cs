﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

	public SmoothFollow cameraFollow;
	public Transform player;
	public float sensitivity = 1f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetMouseButton(0))
		{
			//playerRotation(Input.GetAxis("Mouse X"));
		}
		else if(Input.GetMouseButton(1))
		{
			cameraRotation(Input.GetAxis("Mouse X"));
		}
	}
	
	private void playerRotation(float dir)
	{
		player.transform.Rotate(player.transform.up * dir 
									* sensitivity);
	}
	
	private void cameraRotation(float dir)
	{
		cameraFollow.manualRotationAngle += dir * sensitivity;
	}
}
