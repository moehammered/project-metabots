﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct ClipPoints
{
    public Vector3 botLeft, botRight, topLeft, topRight, center;
}

[RequireComponent(typeof(SmoothFollow))]
public class SmoothFollowCameraOccluder : MonoBehaviour {
    
    public LayerMask mask;
    public bool visualisePoints, visualiseLines, useDelayRedjustment;
    public float adjustmentTime = 0.5f, closestDistanceAllowed = 1.2f, readjustmentDelay = 0.2f;

    private SmoothFollow follow;
    private ClipPoints relativeNearPoints, worldNearPoints;
    private float x, y, z, tanFOV, adjustmentRatio, originalDist, newDist;
    private GameObject[] debugObjects;
    private Vector3[] targetToClips;
    private bool useRaycast = true; 

    private IEnumerator delayNextAdjustment(float time)
    {
//        float currDist = follow.distance;
        float timer = 0;
        while (timer < 1)
        {
            timer += Time.deltaTime/time;
            //modifyDist(Mathf.Lerp(currDist, newDist, timer));
            yield return null;
        }

        useRaycast = true;
    }

    void visualiseCollisionLines()
    {
        Debug.DrawLine(follow.target.position, worldNearPoints.botLeft, Color.green);
        Debug.DrawLine(follow.target.position, worldNearPoints.botRight, Color.green);
        Debug.DrawLine(follow.target.position, worldNearPoints.topLeft, Color.green);
        Debug.DrawLine(follow.target.position, worldNearPoints.topRight, Color.green);
        Debug.DrawLine(follow.target.position, worldNearPoints.center, Color.green);
    }

    void calculateRelativePoints()
    {
        z = Camera.main.nearClipPlane;
       // print(Camera.main.fieldOfView);
        tanFOV = Mathf.Tan(Camera.main.fieldOfView/3.41f);
        x = Mathf.Abs(tanFOV * z);
        //print("TanFOV/2: " + tanFOV + "  x/z: " + x/z);
        y = x/Camera.main.aspect;
        
        relativeNearPoints.botLeft = new Vector3(-x, -y, z);
        relativeNearPoints.botRight = new Vector3(x, -y, z);
        relativeNearPoints.topLeft = new Vector3(-x, y, z);
        relativeNearPoints.topRight = new Vector3(x, y, z);
        relativeNearPoints.center = new Vector3(0, 0, z);
        calculateWorldPoints(ref relativeNearPoints);
    }
    
    void calculateWorldPoints(ref ClipPoints relative)
    {
        worldNearPoints.botLeft = transform.position + (transform.rotation * relative.botLeft);
        worldNearPoints.botRight = transform.position + transform.rotation * relative.botRight;
        worldNearPoints.topLeft = transform.position + transform.rotation * relative.topLeft;
        worldNearPoints.topRight = transform.position + transform.rotation * relative.topRight;
        worldNearPoints.center = transform.position + transform.rotation * relative.center;
    }

    void drawSpherePoints()
    {
        if (visualisePoints)
        {
            debugObjects = new GameObject[5];
            Vector3[] worldPoint = {worldNearPoints.botLeft, 
                                   worldNearPoints.botRight,
                                   worldNearPoints.topLeft,
                                   worldNearPoints.topRight,
                                   worldNearPoints.center};

            for (int i = 0; i < debugObjects.Length; i++)
            {
                debugObjects[i] = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                debugObjects[i].transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
                debugObjects[i].layer = LayerMask.NameToLayer("Ignore Raycast");
                debugObjects[i].transform.position = worldPoint[i];
                debugObjects[i].transform.parent = transform;
            }
        }
    }

    void calculateAdjustmentValues()
    {
        adjustmentRatio = follow.height / follow.distance;
        originalDist = follow.distance;
    }

	// Use this for initialization
	void Start () {
        follow = GetComponent<SmoothFollow>();
        calculateAdjustmentValues();
        calculateRelativePoints();
        drawSpherePoints();
        targetToClips = new Vector3[5];
	}

    void calcDirections()
    {
        targetToClips[0] = worldNearPoints.center - follow.target.position;
        targetToClips[1] = worldNearPoints.botLeft - follow.target.position;
        targetToClips[2] = worldNearPoints.botRight - follow.target.position;
        targetToClips[3] = worldNearPoints.topRight - follow.target.position;
        targetToClips[4] = worldNearPoints.topLeft - follow.target.position;
    }
	
    void delayAdjustment()
    {
        if (useRaycast) //delay method
        {
            if (checkCollision(follow.target))
            {
                useRaycast = false;
                StartCoroutine(delayNextAdjustment(readjustmentDelay));
            }
        }
    }

	// Update is called once per frame
	void FixedUpdate () {
        if (visualiseLines)
            visualiseCollisionLines();
        calculateWorldPoints(ref relativeNearPoints);
        calcDirections();
        if(useDelayRedjustment)
        {
            delayAdjustment();
        }
        else
            checkCollision(follow.target);
	}

    bool checkCollision(Transform target)
    {
        RaycastHit info;
        Ray ray = new Ray(target.position, Vector3.zero);

        foreach(Vector3 dir in targetToClips)
        {
            ray.direction = dir;
            if (Physics.Raycast(ray, out info, follow.distance, mask))
            {
                //print("CORRECT COLLISION!! Because of: " + info.transform.name);
                //print("Distance: " + info.distance);
//                print(dir);
                //modifyDist(info.distance);
                newDist = info.distance;
                //if (follow.distance - newDist > 0.01f)
                {
                    StopAllCoroutines();
                    StartCoroutine(modifyDist(newDist, adjustmentTime));
                    //modifyDist(newDist);
                }
                return true;
            }
        }
        StopAllCoroutines();
        StartCoroutine(modifyDist(originalDist, adjustmentTime));
        //modifyDist(originalDist);
        return false;
    }

    void modifyDist(float dist)
    {
        if (dist < closestDistanceAllowed)
            dist = closestDistanceAllowed;
        follow.distance = dist;
        follow.height = adjustmentRatio * dist;
    }

    IEnumerator modifyDist(float dist, float duration)
    {
        float currDist = follow.distance;
        if (currDist < closestDistanceAllowed)
            currDist = closestDistanceAllowed;
        float timer = 0;
        while (timer < 1)
        {
            timer += Time.deltaTime / duration;
            follow.distance = Mathf.Lerp(currDist, dist, timer);
            follow.height = follow.distance * adjustmentRatio;
            yield return null;
        }

    }
}
