﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LineNoise : MonoBehaviour {

	public CircleLineRenderer line;
	public float magnitude;
	public float waveLife = 0.2f;
	public int currentPoint = 0;
	// Use this for initialization
	void Start () 
	{
		StartCoroutine(createNoise());
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(Input.GetKeyDown(KeyCode.Return))
		{
			//StartCoroutine(createNoise());
			
			visualiseVolume(2);
		}
	}
	
	public void visualiseVolume(float volume)
	{
		magnitude = volume;
		StartCoroutine(startSoundFalloff(waveLife));
	}
	
	private IEnumerator startSoundFalloff(float duration)
	{
		float timer = 0;
		float from = magnitude;
		List<int> indicesModified = new List<int>(line.segments);
		do
		{
			timer += Time.deltaTime/duration;
			magnitude = Mathf.Lerp(from, 0, timer);
			indicesModified.Add(currentPoint);
			yield return null;
		} while(timer < 1);
		magnitude = 0;
		StartCoroutine(removeWave(indicesModified.ToArray()));
	}
	
//	private IEnumerator createChaosNoise()
//	{
//		Vector3[] noisePoints = line.points;
//		float timer = 0.5f;
//		int i = 0;
//		while(timer > 0)
//		{
//			for(int k = 0; k < 6; k+=2)
//			{
//				noisePoints[i+k].y = magnitude * Random.Range (-1f, 1f);
//				line.line.SetPosition(i+k, noisePoints[i+k]);
//			}
//			i+=6;
//			if(i > noisePoints.Length-6)
//			{
//				i = 0;
//			}
//			timer -= Time.deltaTime;
//			yield return null;
//		}
//		
//		for(i = 0; i < noisePoints.Length; i++)
//		{
//			//yield return null;
//			//			noisePoints[i].y = magnitude * Mathf.PerlinNoise( magnitude * Time.time, 0);
//			noisePoints[i].y = 0;
//			line.line.SetPosition(i, noisePoints[i]);
//		}
//		
//		line.points = noisePoints;
//	}

	private IEnumerator removeWave(int[] indices)
	{
		//Vector3[] noisePoints = line.points;
		//print (indices.Length);
		for(int i = 0; i < indices.Length; i++)
		{
			line.points[indices[i]].y = 0;
			//noisePoints[i].y = 0;
			line.line.SetPosition(indices[i], line.points[indices[i]]);
			yield return null;
		}
		
		//line.points = noisePoints;
	}
	
	private IEnumerator createNoise()
	{
		//Vector3[] noisePoints = line.points;
		
		
		int i = 0;
//		while(timer > 0)
//		{
//			noisePoints[i].y = magnitude * Mathf.PerlinNoise( i/(float)noisePoints.Length * Time.time * magnitude, 0);
//			line.line.SetPosition(i, noisePoints[i]);
//			if(i < noisePoints.Length-3)
//			{
//				i+=3;
//			}
//			else
//			{
//				i = 0;
//			}
//			timer -= Time.deltaTime;
//			yield return null;
//		}
		
		for(i = 0; i < line.points.Length; i++)
		{
			yield return null;
			currentPoint = i;
			line.points[i].y = magnitude * Mathf.PerlinNoise( magnitude * Time.time, 0);
			//noisePoints[i].y = 0;
			line.line.SetPosition(i, line.points[i]);
		}
		
		//line.points = noisePoints;
		StartCoroutine(createNoise());
//		line.updatePoints();
	}
}
