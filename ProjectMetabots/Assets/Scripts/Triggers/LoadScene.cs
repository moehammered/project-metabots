﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LoadScene : MonoBehaviour {

	public Image fader;
	public float fadeTime = 1f;
	
	public void restartGame()
	{
		StartCoroutine(fadeOut(0));
		//Application.LoadLevel (Application.loadedLevel);
	}
	
	void Awake()
	{
		if(fader == null)
		{
			GameObject canvas = GameObject.FindObjectOfType<Canvas>().gameObject;
			Image[] images = canvas.GetComponentsInChildren<Image>();
			foreach(Image i in images)
			{
				if(i.name == "FadeImage")
				{
					fader = i;
					break;
				}
			}
		}
	}
	
	private IEnumerator fadeOut(int levelMod)
	{
		float timer = 0;
		Color col = fader.color;
		Time.timeScale = 0;
		while(timer < 1)
		{
			timer += Time.unscaledDeltaTime/fadeTime;
			
			col.a = Mathf.Lerp(0, 1, timer);
			fader.color = col;
			yield return null;
		}
		Time.timeScale = 1;
		Application.LoadLevel(Application.loadedLevel + levelMod);
	}

	public void loadNextScene()
	{
		StartCoroutine(fadeOut(1));
		//Application.LoadLevel(Application.loadedLevel + 1);
	}
	
	public void loadScene(int sceneID)
	{
		Application.LoadLevel (sceneID);
	}
	
	private void Update()
	{
		if(Input.GetKeyUp(KeyCode.Escape))
		{
			Application.Quit ();
		}
	}
	
	private void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.tag == "Player")
		{
			loadNextScene();
		}
	}
}