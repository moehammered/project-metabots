﻿using UnityEngine;
using System.Collections;

public class ClickTip : MonoBehaviour {

	public GameObject playerCamera;
	
	private void Update()
	{
		transform.forward = -(playerCamera.transform.position - transform.position).normalized;
	}
}