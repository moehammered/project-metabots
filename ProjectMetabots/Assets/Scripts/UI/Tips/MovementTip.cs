﻿using UnityEngine;
using System.Collections;

public class MovementTip : MonoBehaviour {
	
	// Disappears when the user successfully uses WSAD.
	private bool wKey;
	private bool sKey;
	private bool dKey;
	private bool aKey;
		
	private void Update()
	{
		if(Input.GetKeyDown (KeyCode.W))
		{
			wKey = true;
		}
		if(Input.GetKeyDown (KeyCode.S))
		{
			sKey = true;
		}
		if(Input.GetKeyDown (KeyCode.D))
		{
			dKey = true;
		}
		if(Input.GetKeyDown (KeyCode.A))
		{
			aKey = true;
		}
		
		if(wKey && sKey && dKey && aKey)
		{
			gameObject.SetActive (false);
		}
	}
}