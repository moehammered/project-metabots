﻿using UnityEngine;
using System.Collections;

public class Patroller : MonoBehaviour {

	public PerceptionSystem system;
//	public MovementStateSystem moveState;
	public PERCEPTION_STATE defaultState = PERCEPTION_STATE.NORMAL;
	public float moveSpeed = 1f;
	private Rigidbody rb;
	// Use this for initialization
	void Start () {
		system = GetComponent<PerceptionSystem>();
		rb = GetComponent<Rigidbody>();
		system.changePerceptionSensitivity(defaultState);
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if(!system.isStunned())
		{
			Vector3 movementVector = transform.forward * moveSpeed;
			rb.MovePosition(transform.position + movementVector * Time.deltaTime);
//			moveState.changeState(MOVE_STATE.RUNNING);
		}
		else
		{
//			moveState.changeState(MOVE_STATE.IDLE);
		}
	}
}
