﻿using UnityEngine;
using System.Collections;

public class EnemyMovementChecker : MonoBehaviour {

	public MovementStateSystem system;
	public AudioSource runSound;
	private Vector3 previousPos, offset;

	void Start () 
	{
		previousPos = transform.position;
	}
	
	void FixedUpdate () 
	{
		checkMovement();
	}
	
	private void checkMovement()
	{
		offset = transform.position - previousPos;
		
		if(offset.sqrMagnitude > 0.0001f) //greater than a speed of 10 units
		{
			system.changeState(MOVE_STATE.RUNNING);
			if(!runSound.isPlaying)
				runSound.Play();
		}
		else
		{
			system.changeState(MOVE_STATE.IDLE);
			runSound.Stop();
		}
		previousPos = transform.position;
	}
}
