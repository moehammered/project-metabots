﻿using UnityEngine;
using System.Collections;

public class EnemyChase : BaseMovement {

	private GameObject player;
	private Patroller patroller;
	private Quaternion previousRotation = Quaternion.identity;
	private Vector3 previousPosition = Vector3.zero;
	private PerceptionSystem state;
	public AudioSource alertSound;
	public float speed = 5f;
	public bool idle = true;

	private void Awake()
	{
		state = GetComponent<PerceptionSystem>();
		patroller = GetComponent<Patroller>();
	}
	
	private void Update()
	{
		if(state.getSensitivity() != PERCEPTION_STATE.STUNNED && idle == false)
		{
			chasePlayer();
		}
	}
	
	private void chasePlayer()
	{
		transform.LookAt(player.transform.position);
		Vector3 newDirection = (player.transform.position - transform.position).normalized;
		move(newDirection * speed * Time.deltaTime);
	}
	
	private IEnumerator returnToPatrol()
	{
		float timeElapsed = 0.0f;
		float rate = (float)1/(speed/2);
		Vector3 startPosition = transform.position;
		transform.LookAt (transform.position + (previousPosition - transform.position));
		
		while(timeElapsed < 1.0f)
		{
			timeElapsed += Time.deltaTime * rate;
			//transform.position = Vector3.Lerp (startPosition, previousPosition, timeElapsed);
			rb.MovePosition(Vector3.Lerp (startPosition, previousPosition, timeElapsed));
			yield return null;
		}
		
		transform.rotation = previousRotation;
		patroller.enabled = true;
		previousPosition = Vector3.zero;
		previousRotation = Quaternion.identity;
		this.enabled = false;
	}
	
	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Player" || other.gameObject.tag == "Player Noise") 
		{
			StopAllCoroutines();
	
			// This check makes sure that the original patrol point location is not overwritten.
			if(previousPosition == Vector3.zero && previousRotation == Quaternion.identity)
			{		
				previousRotation = transform.rotation;
				previousPosition = transform.position;
			}
			
			player = other.gameObject;
			patroller.enabled = false;
			this.enabled = true;
			idle = false;
			//play alert
			alertSound.Play();
		}
	}
	
	private void OnTriggerExit(Collider other)
	{
		if(other.gameObject.tag == "Player")
		{
			idle = true;
			
			if(state.getSensitivity() != PERCEPTION_STATE.STUNNED)
			{
				StartCoroutine(returnToPatrol());
			}
		}
	}
}