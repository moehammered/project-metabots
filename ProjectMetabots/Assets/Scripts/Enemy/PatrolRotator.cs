﻿using UnityEngine;
using System.Collections;

public class PatrolRotator : MonoBehaviour {

	public float rotationAngle = 180f;

	void OnTriggerEnter(Collider col)
	{
		if(col.tag == "Enemy")
		{
			col.transform.Rotate(col.transform.up, rotationAngle);
		}
	}
}
