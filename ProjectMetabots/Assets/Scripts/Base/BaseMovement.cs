﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class BaseMovement : MonoBehaviour {

	protected Rigidbody rb;

	private void Start()
	{
		rb = GetComponent<Rigidbody>();
	}
	
	protected void move(Vector3 direction)
	{
		rb.MovePosition (transform.position + direction);
	}
}