﻿using UnityEngine;
using System.Collections;

public class Lift : BaseMovement {

	public bool playerOn = false;
	public bool activated = false;
	public float speed = 5f;

	private void Update()
	{
		if(playerOn == true)
		{
			if(Input.GetKeyDown(KeyCode.E))
			{
				activated = true;
			}
		}
		
		if(activated == true)
		{
			move (transform.up * speed * Time.deltaTime);
		}
	}
	
	private void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.tag == "LiftStop")
		{
			if(playerOn == true)
			{
				activated = false;
				speed *= -1; // make movement direction reverse
			}
		}
		if(other.gameObject.tag == "Player")
		{
			other.gameObject.transform.parent = transform;
			other.gameObject.GetComponent<Rigidbody>().isKinematic = true;
			playerOn = true;
		}
	}
	
	private void OnTriggerExit(Collider other)
	{
		if(other.gameObject.tag == "Player")
		{
			other.gameObject.transform.parent = null;
			other.gameObject.GetComponent<Rigidbody>().isKinematic = false;
			other.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero; // Stops player from flying off lift.
			playerOn = false;
		}
	}
}