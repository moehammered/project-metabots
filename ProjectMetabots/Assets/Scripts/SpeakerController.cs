﻿using UnityEngine;
using System.Collections;

public class SpeakerController : MonoBehaviour {

	public float noiseRadius = 2f, soundDuration = 1f;
	public NoiseManager manager;
	public AudioSource sound;
	public LineNoise line;
	public bool loop = false;
	// Use this for initialization
	void Start () {
		manager.isStunSound = true;
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Y))
		{
			activateSpeaker();
		}
	}
	
	private IEnumerator countdownStop(float duration)
	{
		line.visualiseVolume(noiseRadius);
		float waveDelay = 0.2f;
		while(duration > 0)
		{
			duration -= Time.deltaTime;
			waveDelay -= Time.deltaTime;
			if(waveDelay < 0)
			{
				manager.increaseNoiseRadius(noiseRadius);
				line.visualiseVolume(noiseRadius);
				waveDelay = 0.2f;
			}
			yield return null;
		}
		
		sound.Stop();
		if(loop)
		{
			activateSpeaker();
		}
	}
	
	public void activateSpeaker()
	{
		if(!sound.isPlaying)
		{
			sound.Play();
			manager.increaseNoiseRadius(noiseRadius);
			if(!line)
			{
				line = GetComponentInChildren<LineNoise>();
			}
			StartCoroutine(countdownStop(soundDuration));
		}
	}
}
