﻿using UnityEngine;
using System.Collections;

public class NoiseManager : MonoBehaviour {

	public bool isStunSound = false;
	public CircleLineRenderer circle;
	
	public void increaseNoiseRadius(float radius)
	{
		StopCoroutine("reduceNoiseRadius");
		transform.localScale = new Vector3(radius, radius, radius);
		circle.createPoints(radius);
		StartCoroutine("reduceNoiseRadius", 0.5f);
	}
	
	private IEnumerator reduceNoiseRadius(float duration)
	{
		float timer = 0;
		
		Vector3 startingScale = transform.localScale;
		
		while(timer < duration)
		{
			timer += Time.deltaTime/duration;
			transform.localScale = Vector3.Lerp(startingScale, Vector3.one, timer);
			circle.createPoints(transform.localScale.x);
			yield return null;
		}
	}
	
	// Use this for initialization
	void Start () 
	{
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Return))
		{
			increaseNoiseRadius(5);
		}
	}
}
