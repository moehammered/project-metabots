﻿using UnityEngine;
using System.Collections;

public class DummyFootsteps : MonoBehaviour {

	public AudioSource sound;
	public float soundRadius = 3f;
	private NoiseManager noiseMaker;
	private LineNoise soundLine;
	private Vector3 previousPos, offset;
	
	void Awake()
	{
		noiseMaker = GetComponentInChildren<NoiseManager>();
		soundLine = GetComponentInChildren<LineNoise>();
	}

	// Use this for initialization
	void Start () 
	{
		previousPos = transform.position;
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		checkMovement();
	}
	
	private void checkMovement()
	{
		offset = transform.position - previousPos;
		
		if(offset.sqrMagnitude > 0.005f) //greater than a speed of 10 units
		{
			if(!sound.isPlaying)
			{
				noiseMaker.increaseNoiseRadius(soundRadius);
				if(!soundLine)
					soundLine = GetComponentInChildren<LineNoise>();
				soundLine.visualiseVolume(soundRadius/2f);
				sound.Play();
			}
		}
		previousPos = transform.position;
	}
}
