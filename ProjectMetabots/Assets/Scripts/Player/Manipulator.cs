﻿using UnityEngine;
using System.Collections;

public class Manipulator : MonoBehaviour {

	private PERCEPTION_STATE effect;
	
	private void Start()
	{
		Destroy(gameObject, 5);
	}
	
	public void setEffect(PERCEPTION_STATE newEffect)
	{
		effect = newEffect;
	}

	private void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.tag == "Enemy")
		{
			PerceptionSystem sys = other.gameObject.GetComponent<PerceptionSystem>();
			if(sys)
			{
				sys.changePerceptionSensitivity(effect);
				Destroy (gameObject);
			}
		}
	}
}