﻿using UnityEngine;
using System.Collections;

public class PlayerStats : MonoBehaviour {
	
	public LoadScene sceneLoader;
	
	private void Awake()
	{
		sceneLoader = GameObject.FindObjectOfType<LoadScene>();
	}
	
	private void OnCollisionEnter(Collision other)
	{
		if(other.gameObject.tag == "Enemy")
		{
			sceneLoader.restartGame();
		}
	}
}