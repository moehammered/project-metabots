﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct MovementKeys
{
	public KeyCode forwardKey;
	public KeyCode backKey;
	public KeyCode rightKey;
	public KeyCode leftKey;
}

public class PlayerMovement : BaseMovement {

	public MovementKeys movementKeys;
	public MovementStateSystem moveSystem;
	public float speed = 5f;
	public bool canStrafe = true, useCameraDir = false, autoRotate = false;
	private SmoothFollow cameraFollow;
	private Vector3 forwDir, rightDir, currentRot, cameraLevelPos;
	
	protected void Awake()
	{
		moveSystem = GetComponent<MovementStateSystem>();
		if(!moveSystem)
			moveSystem = gameObject.AddComponent<MovementStateSystem>();
		cameraFollow = Camera.main.GetComponent<SmoothFollow>();
	}

	private void FixedUpdate()
	{
		checkKeys();
	}
	
	private void LateUpdate()
	{
		if(useCameraDir)
			calcRelativeCamDirection();
		else
		{
			forwDir = transform.forward;
			rightDir = transform.right;
		}
	}

	private void calcRelativeCamDirection()
	{
		cameraLevelPos = Camera.main.transform.position;
		cameraLevelPos.y = transform.position.y;
		forwDir = transform.position - cameraLevelPos;
		forwDir.Normalize();
		if(autoRotate)
		{
			//if the player rotates with the camera
			currentRot = transform.eulerAngles;
			currentRot.y = cameraFollow.manualRotationAngle;
			transform.eulerAngles = currentRot;
			rightDir = transform.right;
		}
		else
		{
			//for if the player doesn't rotate with the camera
			rightDir = transform.right - Camera.main.transform.right;
			rightDir.Normalize();
		}
	}

	private void checkKeys()
	{
		Vector3 newDirection = Vector3.zero;

		if(Input.GetKey (movementKeys.forwardKey))
		{
			newDirection += forwDir;
		}
		if(Input.GetKey (movementKeys.backKey))
		{
			newDirection += -forwDir;
		}
		if(Input.GetKey (movementKeys.rightKey))
		{
			if(canStrafe == true)
			{
				newDirection += rightDir;	
			}
			else
			{
				transform.rotation = Quaternion.AngleAxis(5, Vector3.up) * transform.rotation;
			}
		}
		if(Input.GetKey (movementKeys.leftKey))
		{
			if(canStrafe == true)
			{
				newDirection += -rightDir;	
			}
			else
			{
				transform.rotation = Quaternion.AngleAxis(-5, Vector3.up) * transform.rotation;
			}
		}

		if (newDirection != Vector3.zero) 
		{
			moveSystem.changeState(MOVE_STATE.RUNNING);
			move (newDirection * speed * Time.deltaTime);
		}
		else
		{
			moveSystem.changeState(MOVE_STATE.IDLE);
		}
	}
}
