﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerAttack : MonoBehaviour {

	private PERCEPTION_STATE attackEffect = PERCEPTION_STATE.LIGHT;
	private GameObject enemyTarget;
	public MouseHighlighter selection;
	public Image lightImage;
	public Image soundImage;
	public GameObject manipulator;
	public KeyCode attackKey = KeyCode.Mouse0;
	
	private void Awake()
	{
		selection = Camera.main.GetComponent<MouseHighlighter>();
	}
	
	private void Update()
	{
		if(Input.GetKeyDown (KeyCode.Alpha1))
		{
			attackEffect = PERCEPTION_STATE.LIGHT;
			enableState(lightImage, soundImage);
		}
		else if(Input.GetKeyDown (KeyCode.Alpha2))
		{
			attackEffect = PERCEPTION_STATE.HEARING;
			enableState(soundImage, lightImage);
		}
		
		if(Input.GetKeyDown(attackKey))
		{
			if(selection.currentlyHighlightedObject)
			{
				Vector3 projectileDirection;
			   	RaycastHit hit;
				Ray ray = new Ray(transform.position, selection.currentlyHighlightedObject.transform.position - transform.position);
				if(Physics.Raycast (ray, out hit, 100))
				{
					projectileDirection = (hit.point - transform.position).normalized;
					GameObject manipulatorObject = (GameObject)Instantiate (manipulator.gameObject, transform.position + transform.forward * 3, Quaternion.identity);
					manipulatorObject.GetComponent<Manipulator>().setEffect (attackEffect);
					manipulatorObject.AddComponent<Rigidbody>().AddForce (projectileDirection * 50, ForceMode.Impulse);
				}
			}
		}	
	}
	
	private void enableState(Image enableState, Image disableState)
	{
		Color enableStateColour = enableState.color;
		Color disableStateColour = disableState.color;
		enableStateColour.a = 1f;
		disableStateColour.a = 0.5f;
		enableState.color = enableStateColour;
		disableState.color = disableStateColour;
	}
}