﻿using UnityEngine;
using System.Collections;

public class CharacterAnimController : MonoBehaviour {

	public PerceptionSystem systemSense;
	public MovementStateSystem system;
	public Animator animator;
	private bool isRunning = false;
	private string runParam = "isRunning";
	
	public void playRun()
	{
		if(!isRunning)
		{
			animator.SetBool(runParam, true);
			isRunning = true;
		}
	}	
	
	public void stopRun()
	{
		if(isRunning)
		{
			animator.SetBool(runParam, false);
			isRunning = false;
		}
	}
	
	public void playStun()
	{
		animator.SetBool("isStunned", true);
	}
	
	public void stopStun()
	{
		animator.SetBool("isStunned", false);
	}

	public bool checkRun()
	{
		return isRunning;
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(systemSense)
		{
			if(systemSense.isStunned())
			{
				playStun();
			}
			else if(system.isRunning())
			{
				stopStun();
				playRun();
			}
			else
			{
				stopStun();
				stopRun();
			}
		}
		else
		{
			if(system.isRunning())
			{
				playRun();
			}
			else
			{
				stopRun();
			}
		}
	}
}
