﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(LineRenderer))]
public class CircleLineRenderer : MonoBehaviour
{
	public int segments;
	public float radius;
	public Vector3[] points;
	public LineRenderer line;
	
	void Start ()
	{
		line = gameObject.GetComponent<LineRenderer>();
		points = new Vector3[segments+1];
		line.SetVertexCount (segments + 1);
		line.useWorldSpace = false;
		createPoints ();
		LineNoise eff = gameObject.AddComponent<LineNoise>();
		eff.line = this;
		eff.waveLife = 0.2f;
	}
	
	
	public void createPoints ()
	{
		float x;
		float y;
		
		float angle = 20f;
		
		for (int i = 0; i < (segments + 1); i++)
		{
			x = Mathf.Sin (Mathf.Deg2Rad * angle) * radius;
			y = Mathf.Cos (Mathf.Deg2Rad * angle) * radius;
			
			//points[i] = new Vector3(x,z,y);
			points[i].x = x;
			points[i].z = y;
			line.SetPosition (i, points[i] );
			
			angle += (360f / segments);
		}
	}
	
	public void createPoints (float radius)
	{
		float x;
		float y;
		float z = 0f;
		this.radius = radius;
		float angle = 20f;
		
		for (int i = 0; i < (segments + 1); i++)
		{
			x = Mathf.Sin (Mathf.Deg2Rad * angle) * radius;
			y = Mathf.Cos (Mathf.Deg2Rad * angle) * radius;
			
			points[i].x = x;
			points[i].z = y;
			line.SetPosition (i, points[i] );
			
			angle += (360f / segments);
		}
	}
	
	public void updatePoints()
	{
		for(int i = 0; i < points.Length; i++)
		{
			line.SetPosition(i, points[i]);
		}
	}
}