﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MeshFilter))]
public class CircleMesh : MonoBehaviour {

	public Triangulator triangluator;
	public int segments;
	public float radius;
	public Vector2[] points;
	// Use this for initialization
	void Start () {
		points = new Vector2[segments+1];
		CreatePoints();
		triangluator = new Triangulator(points);
		createMesh();
	}
	
	void createMesh()
	{
		int[] triangles = triangluator.Triangulate();
		Vector2[] uvs = new Vector2[points.Length];
		Vector3[] verts = new Vector3[points.Length];
		
		for(int i = 0; i < verts.Length; i++)
		{
			verts[i] = new Vector3(points[i].x, 0, points[i].y);
			uvs[i] = Vector2.zero;
		}
		
		Mesh mesh = new Mesh();
		mesh.vertices = verts;
		mesh.triangles = triangles;
		mesh.uv = uvs;
		mesh.RecalculateNormals();
		GetComponent<MeshFilter>().mesh = mesh;
	}
	
	void CreatePoints ()
	{
		float x;
		float y;
		float z = 0f;
		
		float angle = 20f;
		
		for (int i = 0; i < (segments + 1); i++)
		{
			x = Mathf.Sin (Mathf.Deg2Rad * angle) * radius;
			y = Mathf.Cos (Mathf.Deg2Rad * angle) * radius;
			
			//line.SetPosition (i,new Vector3(x,z,y) );
			points[i] = new Vector2(x,y);
			angle += (360f / segments);
		}
	}
}
