﻿using UnityEngine;
using System.Collections;

public class QualityController : MonoBehaviour {

	public void setQualityLevel(int level)
	{
		//give it the index of the quality level we want
		//mark true to say we want expensive quality changes to be allowed!
		QualitySettings.SetQualityLevel(level, true);
	}
}
