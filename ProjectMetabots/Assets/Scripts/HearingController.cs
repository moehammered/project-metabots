﻿using UnityEngine;
using System.Collections;

public class HearingController : MonoBehaviour {

	private PerceptionSystem perception;
	private float initialRadius;
	public SphereCollider listenRange;
	
	private void Awake()
	{
		perception = GetComponent<PerceptionSystem>();
		initialRadius = listenRange.radius;
	}
	
	private void Update()
	{
		if(perception.getSensitivity() == PERCEPTION_STATE.HEARING)
		{
			listenRange.radius = initialRadius * 2;
		}
	}
	
	private void OnTriggerEnter(Collider other)
	{
		NoiseManager noise = other.gameObject.GetComponent<NoiseManager>();
		
		if(noise != null && noise.isStunSound == true)
		{	
			if(perception.getSensitivity () == PERCEPTION_STATE.HEARING)
			{
				perception.changePerceptionSensitivity (PERCEPTION_STATE.STUNNED);
			}
		}
	}
}