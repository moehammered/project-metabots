﻿using UnityEngine;
using System.Collections;

public enum MOVE_STATE
{
	NONE,
	IDLE,
	WALKING,
	RUNNING,
	MAX_ENUM_SIZE
};

public class MovementStateSystem : MonoBehaviour {

	private MOVE_STATE currentState;

	void Awake()
	{
		changeState(MOVE_STATE.IDLE);
	}
	
	public MOVE_STATE getState()
	{
		return currentState;
	}
	
	public bool isWalking()
	{
		return currentState == MOVE_STATE.WALKING;
	}
	
	public bool isIdle()
	{
		return currentState == MOVE_STATE.IDLE;
	}
	
	public bool isRunning()
	{
		return currentState == MOVE_STATE.RUNNING;
	}
	
	public void changeState(MOVE_STATE state)
	{
		currentState = state;
	}
}
