﻿using UnityEngine;
using System.Collections;

public class MaterialPerceptionStatus : MonoBehaviour {

	public Color originalColour, stunnedColour = Color.red, 
								sensitiveColour = Color.cyan;
	private Material objectMaterial;
	private PerceptionSystem system;
	
	void Awake()
	{
		system = GetComponent<PerceptionSystem>();
		objectMaterial = GetComponent<Renderer>().material;
	}

	// Use this for initialization
	void Start () 
	{
		originalColour = objectMaterial.color;
	}
	
	// Update is called once per frame
	void Update () {
		checkStatus(system.getSensitivity());
	}
	
	private void checkStatus(PERCEPTION_STATE status)
	{
		switch(status)
		{
			case PERCEPTION_STATE.NORMAL:
				changeColour(originalColour);
			break;
			
			case PERCEPTION_STATE.STUNNED:
				changeColour(stunnedColour);
			break;
			
			default:
				changeColour(sensitiveColour);
			break;
		}
	}
	
	private void changeColour(Color col)
	{
		objectMaterial.color = col;
	}
}
