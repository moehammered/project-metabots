﻿using UnityEngine;
using System.Collections;

public class PerceptionCollider : MonoBehaviour {

	public PERCEPTION_STATE targetState;
	
	void Awake()
	{
		if((int)targetState < 2)
		{
			Debug.LogWarning(this + ": Don't forget to set which state to check for!");
		}
	}

	void OnTriggerEnter(Collider col)
	{
		checkForStun(col);
	}
	
	void OnTriggerStay(Collider col)
	{
		checkForStun(col);
	}
	
	void OnTriggerExit(Collider col)
	{
		checkForStunEnd(col);
	}
	
	private void checkForStun(Collider col)
	{
		if(col.tag == "Enemy")
		{
			PerceptionSystem system = col.gameObject.GetComponent<PerceptionSystem>();
			
			if(system)
			{
				if(system.getSensitivity() == targetState)
				{
					system.stunSenses();
				}
			}
		}
	}
	
	private void checkForStunEnd(Collider col)
	{
		if(col.tag == "Enemy")
		{
			PerceptionSystem system = col.gameObject.GetComponent<PerceptionSystem>();
			
			if(system)
			{
				if(system.isStunned())
				{
					system.stopStun();
				}
			}
		}
	}
}
