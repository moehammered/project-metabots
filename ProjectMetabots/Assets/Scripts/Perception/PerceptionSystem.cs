﻿using UnityEngine;
using System.Collections;

public enum PERCEPTION_STATE
{
	NONE,				//If no state is wanted at all. (non-interactive stuff)
	NORMAL,				//There is no impairment to their senses
	LIGHT,				//Means light is now sensitive
	COLOUR,				//Means colour is now altered
	HEARING,			//Means hearing is now sensitive
	STUNNED,			//Means they are now stunned
	MAX_STATE_AMOUNT 	//used to determine length of sensitivity states
};

public class PerceptionSystem : MonoBehaviour {

	public bool permaStun = false;
	public float stunDuration = 2f;
	//serialised only for debugging, do not modify in other classes.
	//Use the changePerceptionSensitivity function
	[SerializeField]private PERCEPTION_STATE currentState = PERCEPTION_STATE.NORMAL, 
											previousState = PERCEPTION_STATE.NORMAL;

	private void Awake()
	{
		changePerceptionSensitivity(PERCEPTION_STATE.NORMAL);
	}

	public void changePerceptionSensitivity(PERCEPTION_STATE newState)
	{
		previousState = currentState;
		currentState = newState;
	}
	
	public void stunSenses()
	{
		if(!isStunned())
		{
			changePerceptionSensitivity(PERCEPTION_STATE.STUNNED);
			
			if(!permaStun)
			{
				StartCoroutine(countdownStun(stunDuration));
			}
		}
	}
	
	public void returnToNormal()
	{
		changePerceptionSensitivity(PERCEPTION_STATE.NORMAL);
	}
	
	public void stopStun()
	{
		changePerceptionSensitivity(previousState);
	}
	
	public PERCEPTION_STATE getSensitivity()
	{
		return currentState;
	}
	
	public bool isStunned()
	{
		return currentState == PERCEPTION_STATE.STUNNED;
	}
	
	private IEnumerator countdownStun(float duration)
	{
		float timer = duration;
		
		while(timer > 0)
		{
			timer -= Time.deltaTime;
			yield return null;
		}
		
		stopStun();
	}
}
