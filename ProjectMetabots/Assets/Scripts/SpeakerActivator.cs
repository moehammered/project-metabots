﻿using UnityEngine;
using System.Collections;

public class SpeakerActivator : MonoBehaviour {

	public SpeakerController speaker;
	
	private void OnTriggerEnter(Collider other)
	{
		speaker.activateSpeaker();
		
	}
}